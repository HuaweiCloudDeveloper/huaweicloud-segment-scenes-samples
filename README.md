## 仓库简介

此仓库主要用于汇总华为云各产品使用过程中，需使用到java代码调用示例，让用户更为方便快速的使用华为云各类产品及服务。
<br/>

## 项目总览
<table style="text-align: center">
    <tr style="font-weight: bold">
        <td>项目</td>
        <td>介绍</td>
        <td>仓库</td>
    </tr>
    <tr>
        <td>CCE</td>
        <td>通过CCE的Kubernetes api获取集群所有节点信息</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cce-SDK-samples">huaweicloud-cce-SDK-samples</a></td>
    </tr>
    <tr>
        <td rowspan="2">OBS</td>
        <td>将华为云RDS的审计日志转储至华为云对象存储OBS上</td>
        <td rowspan="2"><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-SDK-samples">huaweicloud-obs-SDK-samples</a></td>
    </tr>
    <tr>
        <td>将华为云RDS的慢日志转储至华为云对象存储OBS上</td>
    </tr>
    <tr>
        <td>Kafka</td>
        <td>Kafka自定义消息延时时间</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-kafka-sdk-samples">huaweicloud-kafka-sdk-samples</a></td>
    </tr>
    <tr>
        <td>Redis</td>
        <td>Flink连接Redis进行读取、写入</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-redis-sdk-samples">huaweicloud-redis-sdk-samples</a></td>
    </tr>
</table>
